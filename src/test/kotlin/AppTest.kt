import org.junit.Assert.*
import org.junit.Test

class AppTest {
    @Test
    fun `should return correct output A`() {
        val reportWriter = StringReportWriter()
        val app = App(StringCommandReader("PLACE 0,0,NORTH\n" +
                "MOVE\n" +
                "REPORT"), CommandParser(), reportWriter)

        app.start()

        assertEquals("0,1,NORTH", reportWriter.output)
    }

    @Test
    fun `should return correct output B`() {
        val reportWriter = StringReportWriter()
        val app = App(StringCommandReader("PLACE 0,0,NORTH\n" +
                "LEFT\n" +
                "REPORT"), CommandParser(), reportWriter)

        app.start()

        assertEquals("0,0,WEST", reportWriter.output)
    }

    @Test
    fun `should return correct output C`() {
        val reportWriter = StringReportWriter()
        val app = App(StringCommandReader("PLACE 1,2,EAST\n" +
                "MOVE\n" +
                "MOVE\n" +
                "LEFT\n" +
                "MOVE\n" +
                "REPORT"), CommandParser(), reportWriter)

        app.start()

        assertEquals("3,3,NORTH", reportWriter.output)
    }

    @Test
    fun `should return correct output D`() {
        val reportWriter = StringReportWriter()
        val app = App(StringCommandReader("MOVE\n" +
                "LEFT\n" +
                "REPORT\n" +
                "PLACE 1,2,EAST\n" +
                "REPORT\n" +
                "MOVE\n" +
                "MOVE\n" +
                "LEFT\n" +
                "MOVE\n" +
                "REPORT\n" +
                "PLACE 0,6,SOUTH\n" +
                "REPORT\n" +
                "PLACE 2,3,SOUTH\n" +
                "REPORT"), CommandParser(), reportWriter)

        app.start()

        assertEquals("1,2,EAST3,3,NORTH3,3,NORTH2,3,SOUTH", reportWriter.output)
    }
}