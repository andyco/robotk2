import org.junit.Assert.*
import org.junit.Test
import java.awt.Point

class RobotTest {
    @Test
    fun `should not place on table if beyond north edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 6), Direction.NORTH, table)
        assertFalse(robot.isOnTable)
    }

    @Test
    fun `should not place on table if beyond east edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(6, 0), Direction.NORTH, table)
        assertFalse(robot.isOnTable)
    }

    @Test
    fun `should not place on table if beyond south edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, -1), Direction.NORTH, table)
        assertFalse(robot.isOnTable)
    }

    @Test
    fun `should not place on table if beyond west edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(-1, 0), Direction.NORTH, table)
        assertFalse(robot.isOnTable)
    }

    @Test
    fun `should place on table if within the edge of the table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 0), Direction.NORTH, table)
        assertTrue(robot.isOnTable)
    }

    @Test
    fun `should not move if not on table`() {
        val robot = Robot()
        val table = Table()

        robot.move()
        assertFalse(robot.isOnTable)
        assertEquals(null, robot.location)
        assertEquals(Direction.NORTH, robot.direction)
    }

    @Test
    fun `should not move north if at northern edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 4), Direction.NORTH, table)
        robot.move()
        assertEquals(4, robot.location?.y)
    }

    @Test
    fun `should move north if not at northern edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 3), Direction.NORTH, table)
        robot.move()
        assertEquals(4, robot.location?.y)
    }

    @Test
    fun `should not move east if at eastern edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(4, 0), Direction.EAST, table)
        robot.move()
        assertEquals(4, robot.location?.x)
    }

    @Test
    fun `should move east if not at eastern edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(3, 0), Direction.EAST, table)
        robot.move()
        assertEquals(4, robot.location?.x)
    }

    @Test
    fun `should not move south if at southern edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 0), Direction.SOUTH, table)
        robot.move()
        assertEquals(0, robot.location?.y)
    }

    @Test
    fun `should move south if not at southern edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 1), Direction.SOUTH, table)
        robot.move()
        assertEquals(0, robot.location?.y)
    }

    @Test
    fun `should not move west if at western edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 0), Direction.WEST, table)
        robot.move()
        assertEquals(0, robot.location?.x)
    }

    @Test
    fun `should move west if not at western edge of table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(1, 0), Direction.WEST, table)
        robot.move()
        assertEquals(0, robot.location?.x)
    }

    @Test
    fun `should turn left if on table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 0), Direction.NORTH, table)
        robot.left()
        assertEquals(Direction.WEST, robot.direction)

        robot.left()
        assertEquals(Direction.SOUTH, robot.direction)

        robot.left()
        assertEquals(Direction.EAST, robot.direction)

        robot.left()
        assertEquals(Direction.NORTH, robot.direction)
    }

    @Test
    fun `should turn right if on table`() {
        val robot = Robot()
        val table = Table()

        robot.placeOnTable(Point(0, 0), Direction.NORTH, table)
        robot.right()
        assertEquals(Direction.EAST, robot.direction)

        robot.right()
        assertEquals(Direction.SOUTH, robot.direction)

        robot.right()
        assertEquals(Direction.WEST, robot.direction)

        robot.right()
        assertEquals(Direction.NORTH, robot.direction)
    }
}