import org.junit.Assert.*
import org.junit.Test

class CommandParserTest {

    @Test
    fun `should parse multiline strings`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("PLACE 1,2,WEST\n" +
                                                        "MOVE\n" +
                                                        "LEFT\n" +
                                                        "RIGHT\n" +
                                                        "REPORT")

        assertEquals(5, commands.size)
        assertTrue(commands.elementAt(0) is PlaceCommand)
        assertTrue(commands.elementAt(1) is MoveCommand)
        assertTrue(commands.elementAt(2) is LeftCommand)
        assertTrue(commands.elementAt(3) is RightCommand)
        assertTrue(commands.elementAt(4) is ReportCommand)

        val placeCommand = commands.elementAt(0) as PlaceCommand
        assertEquals(1,placeCommand.location.x)
        assertEquals(2,placeCommand.location.y)
        assertEquals(Direction.WEST, placeCommand.direction)
    }

    @Test
    fun `should return UnknownCommand for unknown commands`() {
        val parser = CommandParser()

        var commands = parser.parseCommands("GIBBERISH")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is UnknownCommand)

        commands = parser.parseCommands("MOVED")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is UnknownCommand)

        commands = parser.parseCommands("MOVE D")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is UnknownCommand)

        commands = parser.parseCommands("AMOVE")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is UnknownCommand)

        commands = parser.parseCommands("A MOVE")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is UnknownCommand)
    }


    @Test
    fun `should return unknown command for empty lines`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("PLACE 1,2,WEST\r\n" +
                                                        "\r\n" +
                                                        "LEFT\r\n")

        assertEquals(3, commands.size)
        assertTrue(commands.elementAt(0) is PlaceCommand)
        assertTrue(commands.elementAt(1) is UnknownCommand)
        assertTrue(commands.elementAt(2) is LeftCommand)
    }

    @Test
    fun `should return no commands when supplied string is blank`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("")

        assertEquals(0, commands.size)
    }

    @Test
    fun `should return move command`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("MOVE")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is MoveCommand)
    }

    @Test
    fun `should return move command surrounded by whitespace`() {
        val parser = CommandParser()

        val commands = parser.parseCommands(" MOVE ")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is MoveCommand)
    }

    @Test
    fun `should return left command`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("LEFT")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is LeftCommand)
    }

    @Test
    fun `should return left command surrounded by whitespace`() {
        val parser = CommandParser()

        val commands = parser.parseCommands(" LEFT ")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is LeftCommand)
    }

    @Test
    fun `should return right command`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("RIGHT")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is RightCommand)
    }

    @Test
    fun `should return right command surrounded by whitespace`() {
        val parser = CommandParser()

        val commands = parser.parseCommands(" RIGHT ")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is RightCommand)
    }

    @Test
    fun `should return report command`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("REPORT")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is ReportCommand)
    }

    @Test
    fun `should return report command surrounded by whitespace`() {
        val parser = CommandParser()

        val commands = parser.parseCommands(" REPORT ")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is ReportCommand)
    }

    @Test
    fun `should return place command`() {
        val parser = CommandParser()

        val commands = parser.parseCommands("PLACE 3,4,SOUTH")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is PlaceCommand)

        val placeCommand = commands.elementAt(0) as PlaceCommand
        assertEquals(3,placeCommand.location.x)
        assertEquals(4,placeCommand.location.y)
        assertEquals(Direction.SOUTH, placeCommand.direction)
    }

    @Test
    fun `should return place command surrounded by whitespace`() {
        val parser = CommandParser()

        val commands = parser.parseCommands(" PLACE 3,4,SOUTH ")

        assertEquals(1, commands.size)
        assertTrue(commands.elementAt(0) is PlaceCommand)

        val placeCommand = commands.elementAt(0) as PlaceCommand
        assertEquals(3,placeCommand.location.x)
        assertEquals(4,placeCommand.location.y)
        assertEquals(Direction.SOUTH, placeCommand.direction)
    }
}