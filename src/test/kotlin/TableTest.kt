import org.junit.Assert.*
import org.junit.Test
import java.awt.Point

internal class TableTest {

    @Test
    fun canPlaceRobotAt() {
        val table = Table()
        val result = table.canPlaceRobotAt(Point(0,0))
        assertTrue(result)
    }

    @Test
    fun getSize() {
        val table = Table()
        val result = table.size
        assertEquals(5, result)
    }
}