import java.awt.Point

/**
 * Represents a table
 */
class Table(val size: Int = 5) {

    /**
     * Determines whether the Robot can be placed at the specified location
     *
     * @property location The location where the robot will be placed.
     */
    fun canPlaceRobotAt(location: Point) : Boolean {

        return location.x >= 0 && location.x < this.size && location.y >= 0 && location.y < this.size
    }
}