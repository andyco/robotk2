fun main(args: Array<String>) {
    val app = App(StandardInputCommandReader(), CommandParser(), StandardOutputReportWriter())
    app.start()
}