import java.awt.Point

class Robot {
    private var table : Table? = null

    var direction: Direction = Direction.NORTH

    var location: Point? = null

    val isOnTable: Boolean
        get() = this.table != null

    fun placeOnTable(location: Point, direction: Direction, table: Table) {
        if (table.canPlaceRobotAt(location)) {
            this.table = table
            this.location = location
            this.direction = direction
        }
    }

    fun move() {
        if (!isOnTable) {
            return
        }

        val x = when(this.direction) {
            Direction.EAST -> this.location!!.x + 1
            Direction.WEST -> this.location!!.x - 1
            else -> this.location!!.x
        }

        val y = when(this.direction) {
            Direction.NORTH -> this.location!!.y + 1
            Direction.SOUTH -> this.location!!.y - 1
            else -> this.location!!.y
        }

        val newLocation = Point(x, y)

        if (this.table!!.canPlaceRobotAt(newLocation)) {
            this.location = newLocation
        }
    }

    fun left() {
        if (!isOnTable) {
            return
        }

        this.direction = Direction.fromInt((this.direction.value - 90 + 360) % 360)
    }

    fun right() {
        if (!isOnTable) {
            return
        }

        this.direction = Direction.fromInt((this.direction.value + 90) % 360)
    }
}