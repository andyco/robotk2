/**
 * The directions the robot may be facing
 */
enum class Direction(val value: Int) {
    NORTH(0),
    EAST(90),
    SOUTH(180),
    WEST(270);

    companion object {
        fun fromInt(value: Int) = Direction.values().first { it.value == value }
    }
}