/**
 * String based report writer used for testing.
 */
class StringReportWriter : IReportWriter {
    var output : String = ""

    override fun writeReport(x : Int, y : Int, direction : Direction)
    {
        output += "$x,$y,$direction"
    }
}