class StandardOutputReportWriter : IReportWriter {

    override fun writeReport(x : Int, y : Int, direction : Direction)
    {
        println("$x,$y,$direction")
    }
}