import java.util.*

/**
 * Interface for a class that implements a command parser
 */
interface ICommandParser {

    /**
     * Parses commands supplied as a string into a Queue of ICommands
     * @param commands A string of commands where each command is on a new line.
     */
    fun parseCommands(commands: String) : Queue<ICommand>
}