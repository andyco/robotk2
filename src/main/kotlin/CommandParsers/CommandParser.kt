import java.util.*
import java.awt.Point
//import com.github.h0tk3y.betterParse.combinators.*
//import com.github.h0tk3y.betterParse.grammar.Grammar
//import com.github.h0tk3y.betterParse.grammar.parseToEnd

//operator fun Regex.contains(text: CharSequence): Boolean = this.matches(text)

/**
 * Parses commands
 */
class CommandParser : ICommandParser {

    override fun parseCommands(commands: String) : Queue<ICommand> {
        val commandQueue: Queue<ICommand> = LinkedList<ICommand>()
        val placeRegexPattern = "^PLACE (\\d+),(\\d+),(NORTH|SOUTH|EAST|WEST)$"

        if (commands.isNotBlank()) {
            //commandQueue.addAll(CommandParser.parseToEnd(commands.trim()))

            for(line in commands.trim().lineSequence()) {

                when(line.trim()) {
                    "LEFT" -> commandQueue.add(LeftCommand())
                    "RIGHT" -> commandQueue.add(RightCommand())
                    "MOVE" -> commandQueue.add(MoveCommand())
                    "REPORT" -> commandQueue.add(ReportCommand())
                    else -> {
                        val match = Regex(placeRegexPattern).matchEntire(line)
                        if (match != null) {
                            val x = match.groups[1]!!.value.toInt()
                            val y = match.groups[2]!!.value.toInt()
                            val dir = Direction.valueOf(match.groups[3]!!.value)
                            commandQueue.add(PlaceCommand(Point(x, y), dir))
                        } else {
                            commandQueue.add(UnknownCommand())
                        }
                    }
                }
            }
        }

        return commandQueue
    }

//    object CommandParser : Grammar<List<ICommand>>() {
//        val left by token("LEFT\\b")
//        val right by token("RIGHT\\b")
//        val move by token("MOVE\\b")
//        val report by token("REPORT\\b")
//        val place by token("PLACE[ ]")
//        val number by token("\\d+")
//        val comma by token(",")
//        val direction by token("(NORTH|SOUTH|EAST|WEST)")
//        val any by token(".+")
//        val newline by token("[\r\n]+", ignore = true)
//
//        val leftParser by left use { LeftCommand() }
//        val rightParser by right use { RightCommand() }
//        val moveParser by move use { MoveCommand() }
//        val reportParser by report use { ReportCommand() }
//        val placeParser by place and number and comma and number and comma and direction map { (_, x, _, y, _, dir) ->
//            PlaceCommand(
//                Point(x.text.toInt(), y.text.toInt()),
//                Direction.valueOf(dir.text)
//            )
//        }
//        val unknownParser by any use { UnknownCommand() }
//
//        override val rootParser by separatedTerms(
//            placeParser or moveParser or leftParser or rightParser or reportParser or unknownParser,
//            newline
//        )
//    }
}