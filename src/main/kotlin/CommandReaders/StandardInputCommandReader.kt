class StandardInputCommandReader : ICommandReader {

    init {
        println("Press Ctrl + C to exit")
        println("Enter Command(s):")
    }

    override var canRead: Boolean = true

    override fun readLine(): String {
        return kotlin.io.readLine().orEmpty()
    }
}