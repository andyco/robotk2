interface ICommandReader {

    var canRead: Boolean

    fun readLine(): String
}