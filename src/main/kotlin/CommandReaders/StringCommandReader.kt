/**
 * String based command reader used for testing.
 */
class StringCommandReader(private val commands : String) : ICommandReader {

    override var canRead: Boolean = true

    override fun readLine(): String {
        canRead = false
        return commands
    }
}