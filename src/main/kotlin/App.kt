/**
 * Implements the main application logic.
 */
class App(private val commandReader : ICommandReader, private val commandParser : ICommandParser, private val reportWriter : IReportWriter) {

    private val robot : Robot = Robot()
    private val table : Table = Table()

    fun start(){
        while(commandReader.canRead)
        {
            val commandString = commandReader.readLine()
            val commandQueue = commandParser.parseCommands(commandString)

            while (commandQueue.peek() != null)
            {
                val command = commandQueue.remove()
                when (command)
                {
                    is PlaceCommand -> robot.placeOnTable(command.location, command.direction, table)

                    is MoveCommand -> robot.move()

                    is LeftCommand -> robot.left()

                    is RightCommand -> robot.right()

                    is ReportCommand -> if (robot.isOnTable)
                                        {
                                            reportWriter.writeReport(robot.location!!.x, robot.location!!.y, robot.direction)
                                        }
                }
            }
        }
    }
}